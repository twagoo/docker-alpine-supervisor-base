#!/bin/bash

#Delete any file older than 10 days from /var/log (except for .pos files which are needed by fluentd)
find /var/log -mtime +10 -type f -not -name '*.pos' -delete
