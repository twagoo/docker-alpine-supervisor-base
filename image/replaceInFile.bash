#!/bin/bash


#Escape forward slashes in any replacement value
VAR=$(printf '%s\n' "$1" | sed -e 's/[]\/$*.^[]/\\&/g');
VALUE=$(printf '%s\n' "$2" | sed -e 's/[\/&]/\\&/g')
FILE="$3"

echo "  Replacing [$VAR] with [$VALUE]"
sed -i "s/$VAR/$VALUE/g" "$FILE"
